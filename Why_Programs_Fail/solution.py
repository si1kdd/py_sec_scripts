#!/usr/bin/env python2

import sys
import os
import subprocess

count = 0


def test_xml(parse_files):
    input_file = open(parse_files, "r")
    ch = input_file.read()
    test_func(ch)
    input_file.close()


def write_xml(t):
    xml = open("input.xml", "w")
    wstr = ""
    for (index, char) in t:
        wstr += char
    xml.write(wstr)
    xml.close()
    return wstr


def string_to_tuple_list(s):
    l = []
    for i in range(len(s)):
        l.append((i, s[i]))
    return l



def test_func(ch):
    content = write_xml(ch)
    global count
    count += 1

    print "Testing %d : %s" % (count, content)
    try:
        status = subprocess.call(["python2", "xpcmd.py", "input.xml"], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if status:
            print "[!] Fail"
            return "FAIL"
        else:
            print "[*] Pass"
            return "PASS"
    except SyntaxError: # XML syntax.
        return UNRESOLVED
